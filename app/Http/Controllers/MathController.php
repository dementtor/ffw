<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MathController extends Controller
{
    /**
     * Calculate some equation
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function calculate(): \Illuminate\Http\JsonResponse {
        $attributes = request()->validate([
            'expression' => 'required'
        ]);

        // Find out how manu numbers we have into the string.
        preg_match_all('#\d+#', $attributes['expression'], $check_numbers);
        // We can make simple equations, thus we need at least 2 numbers to work with.
        if(empty($check_numbers[0])) {
            return response()->json([
                'valid' => FALSE,
                'reason' => 'Non-math questions ("' . $attributes['expression'] . '")'
            ]);
        } elseif(preg_match('#^What is \d+\?#', $attributes['expression'])) {
            // Since we need to return int and just to save some time on again finding the number,
            // we cast and reuse the found numbers from previous match
            return response()->json([
                'result' => (int)reset($check_numbers[0])
            ]);
        }

        // Remove the pesky question sign since we don't need it.
        $problem = rtrim($attributes['expression'],'?');

        $problem = explode(' ', $problem);
        $math = [];

        // Find what the math problem might be.
        foreach ($problem as $value) {
            if(is_numeric($value)) {
                $math[] = (int)$value;
            } else {
                // Make some matching to have idea of what operations we might need to use
                switch ($value) {
                    case 'plus':
                        $math[] = 'plus';
                    break;
                    case 'minus':
                        $math[] = 'minus';
                    break;
                    case 'multiplied':
                        $math[] = 'multiplied';
                    break;
                    case 'divided':
                        $math[] = 'divided';
                    break;
                    default:
                    break;
                };
            }
        }

        // If we don't have any operation bail out.
        if(!isset($math[1])) {
            return response()->json([
                'valid' => FALSE,
                'reason' => 'Unsupported operations ("' . $attributes['expression'] . '")'
            ]);
        }

        // Basic arithmetics
        // We know that for a basic equation to work we have always 2 numbers and an operator,
        // thus the array should always be with length 3
        if(count($math) === 3) {
            return response()->json([
                'result' => $this->basicArithmetics($math[0], $math[1], $math[2])
            ]);
        } elseif(count($math) === 5) {// Complicated arithmetics
            // We know that for the complicated equation, there always will be 3 numbers involved and 2 operations to work with,
            // thus the length of the array can only be 5 elements
            return response()->json([
                'result' => $this->complicatedArithmetics($math)
            ]);
        } else {
            return response()->json([
                'valid' => FALSE,
                'reason' => 'Something is wrong, please check your equation',
            ]);
        }
    }

    /**
     * This method will handle the more complicated calculations
     *
     * @param array $equation
     * @param $calc
     * @return float|int
     */
    protected function complicatedArithmetics(array $equation, $calc = NULL): float|int {
        if(count($equation) > 3) {
            $math = array_slice($equation, 0, 3);
            array_splice($equation, 0, 3);
        }

        if(!empty($math)) {
            $result = $this->basicArithmetics($math[0], $math[1], $math[2]);
            $result = $this->complicatedArithmetics($equation, $result);
        } else {
            $result = $this->basicArithmetics($calc, $equation[0], $equation[1]);
        }

        return $result;
    }

    /**
     * This method will handle the basic arithmetics.
     *
     * @param $a
     * @param $operation
     * @param $b
     * @return float|int
     */
    protected function basicArithmetics($a, $operation, $b): float|int {
        try {
            $result = match($operation) {
                'plus' => $a + $b,
                'minus' => $a - $b,
                'multiplied' => $a * $b,
                'divided' => $a / $b,
            };
        } catch (\Exception $e) {
            // Keep it quite, no one likes people who talk too much.
        }

        return $result;
    }

    /**
     * Validate equations.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function validateEquation(): \Illuminate\Http\JsonResponse {
        $attributes = request()->validate([
            'expression' => 'required'
        ]);
        preg_match_all('#\d+#', $attributes['expression'], $check_numbers);

        if(empty($check_numbers[0])) {
            return response()->json([
                'valid' => FALSE,
                'reason' => 'Non-math questions ("' . $attributes['expression'] . '")'
            ]);
        } elseif(preg_match('#^What is \d+\?#', $attributes['expression'])) {
            return response()->json([
                'valid' => TRUE
            ]);
        } elseif(!str_contains($attributes['expression'], 'plus')
            && !str_contains($attributes['expression'], 'minus')
            && !str_contains($attributes['expression'], 'multiplied')
            && !str_contains($attributes['expression'], 'divided')
        ) {
            return response()->json([
                'valid' => FALSE,
                'reason' => 'Unsupported operations ("' . $attributes['expression'] . '")'
            ]);
        } elseif(substr_count($attributes['expression'], 'plus') > 1
            || substr_count($attributes['expression'], 'minus') > 1
            || substr_count($attributes['expression'], 'multiplied') > 1
            || substr_count($attributes['expression'], 'divided') > 1
        ) {
            return response()->json([
                'valid' => FALSE,
                'reason' => 'Expressions with invalid syntax ("' . $attributes['expression'] . '")'
            ]);
        } else {
            return response()->json([
                'valid' => TRUE
            ]);
        }
    }
}

<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About this mathematical tasks

In this task, I have decided to use Laravel instead of just play Drupal, since to only 2 routes and few calculations is way too much of an overkill to install a whole CMS.
The approach I tool with the task in hand was to make at least two different types on implementation on purpose for the two endpoints, just to make the whole thing a bit more exotic.

## How to work with it.
In order to test the project you can use my temporary deployed project which URL is described in the email I have sent it already.
The endpoints are located at /api/evaluate and /api/validate. As requested they only accept POST request.

Of course for simplicity reasons the API does not require any authentication but if it was going to be production ready, I would go and add a middleware there with either basic authentication, token authentication or something else if required.

If you have any further questions feel free to get in touch with me.

Thank you for your time.
